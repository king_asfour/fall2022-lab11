package com.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TestSquare {
    @Test
    public void testConstructor() {
        Square mySquare = new Square(5);
        assertNotNull(mySquare);
        assertEquals(5, mySquare.getLength(),1);
        assertEquals(5, mySquare.getWidth(),1);
    }
}