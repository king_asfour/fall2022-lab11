package com.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TestCircle {

    @Test
    public void testConstructor() {
        Circle myCircle = new Circle(9);
        assertNotNull(myCircle);
        assertEquals(9, myCircle.getRadius(),1);
    }

    @Test
    public void testGetArea() {
        Circle myCircle = new Circle(3);
        assertEquals(28.2743, myCircle.getArea(),0.1); ;
    }

    @Test
    public void testGetPerimeter() {
        Circle myCircle = new Circle(5);
        assertEquals(31.4159, myCircle.getPerimeter(),0.1);
    }
}
