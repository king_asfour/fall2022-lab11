package com.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TestRectangle {
    @Test
    public void testConstructor() {
        Rectangle myBoi = new Rectangle(5, 9);
        assertNotNull(myBoi);
        assertEquals(5, myBoi.getLength(),1);
        assertEquals(9, myBoi.getWidth(),1);
    }

    @Test
    public void testGetArea() {
        Rectangle myBoi = new Rectangle(5, 9);
        assertEquals(45, myBoi.getArea(),1);
    }

    @Test
    public void testGetPerimeter() {
        Rectangle myBoi = new Rectangle(5, 9);
        assertEquals(28, myBoi.getPerimeter(),1);
    }
}
