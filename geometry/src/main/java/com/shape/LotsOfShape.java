package com.shape;

public class LotsOfShape {
    public static void main(String[] args) {
        Shape[] myShapes = new Shape[5];
    //Hardcode Shapes/Create shapes
        myShapes[0] = new Rectangle(6, 7);
        myShapes[1] = new Rectangle(6,9);
        myShapes[2] = new Circle(10); 
        myShapes[3] = new Circle(3);
        myShapes[4] = new Square(9);

        for(Shape s: myShapes) {
            System.out.println("Area of a " + s.getShape() +  " is: " + s.getArea() );
            System.out.println("Perimeter of a "+ s.getShape() + " is: " + s.getPerimeter());
        }
    }
}
