package com.shape;

public class Square extends Rectangle{
    
    public Square(double sideLength) {
        super(sideLength,sideLength);
    }

    @Override
    public String getShape() {
        return "Square";
    }
}
